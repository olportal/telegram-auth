//
//  TARequest.h
//  telegram-temp
//
//  Created by Дмитрий Шелонин on 28/09/2017.
//  Copyright © 2017 Дмитрий Шелонин. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TARequest : NSObject
@property id ident;
@property int resendCount;

- (void)cancel;
@end
