//
//  TelegramAuth.h
//  telegram-temp
//
//  Created by Дмитрий Шелонин on 28/09/2017.
//  Copyright © 2017 Дмитрий Шелонин. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TARequest.h"

typedef enum : NSUInteger {
    TAConfirmViaTelegram
    , TAConfirmViaPhone
    , TAConfirmViaOther
} TAConfirmVia;

typedef enum : NSUInteger {
    TAErrorTypeSendCode
    , TAErrorTypeConfirmCode
} TAErrorType;

typedef enum : NSUInteger {
    TAErrorCodeUnknown
    , TAErrorCodeInvalidPhoneNumber
    , TAErrorCodeFloodWait
    , TAErrorCodeFloodPhone
    , TAErrorCodeInvalidPhoneCode
    , TAErrorCodePhoneCodeExpired
    , TAErrorCodePhoneNumberUnoccupied
    , TAErrorCodeSessionPasswordNeeded
} TAErrorCode;

static NSString *const kTelegramAuthErrorCode = @"code";
static NSString *const kTelegramAuthErrorName = @"name";
static NSString *const kTelegramAuthErrorType = @"type";

@interface TAConfirm : NSObject
    @property NSString *code;
    @property NSString *phone;
    @property NSString *phoneHash;
    @property NSString *userId;
    @property NSString *userName;
    @property BOOL phoneRegistered;
    @property TAConfirmVia confirmedVia;
@end

static NSString *const TAErrorDomain = @"telegram.auth.error";

typedef void(^TASuccessBlock)(TAConfirm *result);
typedef void(^TAFailBlock)(NSError *error);

@interface TelegramAuth : NSObject
+ (instancetype)sharedInstance;

- (TARequest *)sendCodeToPhone:(NSString *)phoneNumber success:(TASuccessBlock)success fail:(TAFailBlock)fail;
- (TARequest *)confirmCode:(TAConfirm *)confirmInfo success:(TASuccessBlock)success fail:(TAFailBlock)fail;
@end
