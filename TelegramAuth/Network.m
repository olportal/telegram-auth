//
//  Network.m
//  telegram-temp
//
//  Created by Дмитрий Шелонин on 26/09/2017.
//  Copyright © 2017 Дмитрий Шелонин. All rights reserved.
//

#import "Network.h"
#import "TLRPCauth_sendCode.h"
#import "RPC.h"
#import "TLauth_SentCode.h"
#import "TLRPCauth_signIn.h"

@interface Network ()
@end

@implementation Network

+ (instancetype)shared {
    static Network *shared = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shared = [self new];
    });
    
    return shared;
}

- (NSObject *)doSendConfirmationCode:(NSString *)phoneNumber success:(void (^)(id))success fail:(void (^)(MTRpcError *))fail {
    TLRPCauth_sendCode *sendCode = [[TLRPCauth_sendCode alloc] init];
    sendCode.flags = 0;
    sendCode.phone_number = phoneNumber;
    sendCode.api_id = apiId;
    sendCode.api_hash = apiHash;
    sendCode.lang_code = [NSLocale preferredLanguages].firstObject;
    
    return [[RPC shared] performRpc:sendCode completionBlock:^(id<TLObject> response, int64_t responseTime, MTRpcError *error) {
        if (error == nil)
        {
            if (success) {
                success((TLauth_SentCode *)response);
            }
        }
        else
        {
            if (fail) {
                fail(error);
            }
        }
    } progressBlock:nil quickAckBlock:nil requiresCompletion:YES requestClass:RPCRequestClassGeneric | RPCRequestClassFailOnServerErrors | RPCRequestClassFailOnFloodErrors datacenterId:INT_MAX];
}

- (NSObject *)doSignIn:(NSString *)phoneNumber phoneHash:(NSString *)phoneHash phoneCode:(NSString *)phoneCode success:(void (^)(id))success fail:(void (^)(MTRpcError *))fail {
    TLRPCauth_signIn$auth_signIn *signIn = [[TLRPCauth_signIn$auth_signIn alloc] init];
    signIn.phone_number = phoneNumber;
    signIn.phone_code_hash = phoneHash;
    signIn.phone_code = phoneCode;
    
    return [[RPC shared] performRpc:signIn completionBlock:^(id<TLObject> response, int64_t responseTime, MTRpcError *error) {
        if (error == nil)
        {
            if (success) {
                success((TLauth_SentCode *)response);
            }
        }
        else
        {
            if (fail) {
                fail(error);
            }
        }
    } progressBlock:nil quickAckBlock:nil requiresCompletion:YES requestClass:RPCRequestClassGeneric | RPCRequestClassFailOnServerErrors | RPCRequestClassFailOnFloodErrors | RPCRequestClassPassthroughPasswordNeeded datacenterId:INT_MAX];
}

@end
