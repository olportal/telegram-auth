//
//  TelegramAuth.h
//  TelegramAuth
//
//  Created by Дмитрий Шелонин on 29/09/2017.
//  Copyright © 2017 Дмитрий Шелонин. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for TelegramAuth.
FOUNDATION_EXPORT double TelegramAuthVersionNumber;

//! Project version string for TelegramAuth.
FOUNDATION_EXPORT const unsigned char TelegramAuthVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <TelegramAuth/PublicHeader.h>


#import <TelegramAuth/TA.h>
