//
//  RPC.h
//  telegram-temp
//
//  Created by Дмитрий Шелонин on 26/09/2017.
//  Copyright © 2017 Дмитрий Шелонин. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import <MtProtoKit/MtProtoKit.h>
#import <MtProtoKitDynamic/MtProtoKitDynamic.h>

@class TLMetaRpc;

@protocol TLObject;

typedef enum {
    RPCRequestClassGeneric = 1,
    RPCRequestClassDownloadMedia = 2,
    RPCRequestClassUploadMedia = 4,
    RPCRequestClassEnableUnauthorized = 8,
    RPCRequestClassEnableMerging = 16,
    RPCRequestClassHidesActivityIndicator = 64,
    RPCRequestClassLargeMedia = 128,
    RPCRequestClassFailOnServerErrors = 256,
    RPCRequestClassFailOnFloodErrors = 512,
    RPCRequestClassPassthroughPasswordNeeded = 1024,
    RPCRequestClassIgnorePasswordEntryRequired = 2048,
    RPCRequestClassFailOnServerErrorsImmediately = 4096,
} RPCRequestClass;

@interface RPC : NSObject

+ (instancetype)shared;

- (NSObject *)performRpc:(TLMetaRpc *)rpc
         completionBlock:(void (^)(id<TLObject> response, int64_t responseTime, MTRpcError *error))completionBlock
           progressBlock:(void (^)(int length, float progress))__unused progressBlock
           quickAckBlock:(void (^)())quickAckBlock
      requiresCompletion:(bool)__unused requiresCompletion
            requestClass:(int)requestClass
            datacenterId:(int)datacenterId;
- (void)moveToDatacenterId:(NSInteger)datacenterId;
- (void)cancelRPC:(id)rpc;
@end

@interface MTRequest (LegacyTL)

- (void)setBody:(TLMetaRpc *)body;

@end
