//
//  Network.h
//  telegram-temp
//
//  Created by Дмитрий Шелонин on 26/09/2017.
//  Copyright © 2017 Дмитрий Шелонин. All rights reserved.
//

#import <Foundation/Foundation.h>

@class MTRpcError;

@interface Network : NSObject
+ (instancetype)shared;

- (NSObject *)doSendConfirmationCode:(NSString *)phoneNumber success:(void (^)(id object))success fail:(void (^)(MTRpcError *error))fail;
- (NSObject *)doSignIn:(NSString *)phoneNumber phoneHash:(NSString *)phoneHash phoneCode:(NSString *)phoneCode success:(void (^)(id object))success fail:(void (^)(MTRpcError *error))fail;
@end
