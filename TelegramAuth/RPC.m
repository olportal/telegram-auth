//
//  RPC.m
//  telegram-temp
//
//  Created by Дмитрий Шелонин on 26/09/2017.
//  Copyright © 2017 Дмитрий Шелонин. All rights reserved.
//

#import "RPC.h"
#import "TLMetaRpc.h"
#import "TGTLSerialization.h"
#import "TLRPCauth_sendCode.h"

@implementation MTRequest (LegacyTL)

- (void)setBody:(TLMetaRpc *)body
{
    [self setPayload:[TGTLSerialization serializeMessage:body] metadata:body responseParser:^id(NSData *data)
     {
         return [TGTLSerialization parseResponse:data request:body];
     }];
}

- (id)body
{
    return self.metadata;
}

@end

@interface RPC ()
@property MTContext *context;
@property MTRequestMessageService *requestService;
@property MTProto *mtProto;
@property (nonatomic, readonly) id<MTKeychain> keychain;
@end

@implementation RPC

@synthesize keychain = _keychain;

+ (instancetype)shared {
    static RPC *rpc = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        rpc = [self new];
    });
    
    return rpc;
}

+ (NSString *)documentsPath {
    static NSString *path = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSString *groupName = [@"group." stringByAppendingString:[[NSBundle mainBundle] bundleIdentifier]];

        NSURL *groupURL = [[NSFileManager defaultManager] containerURLForSecurityApplicationGroupIdentifier:groupName];
        if (groupURL != nil) {
            NSString *documentsPath = [[groupURL path] stringByAppendingPathComponent:@"Documents"];

            [[NSFileManager defaultManager] createDirectoryAtPath:documentsPath withIntermediateDirectories:true attributes:nil error:NULL];

            path = documentsPath;
        } else
            path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, true)[0];
    });
    
    return path;
}

+ (NSDictionary *)seedAddressesForDatacenters {
    static NSDictionary *addresses = nil;
    if (!addresses) {
        addresses = @{
                      @1: @{
                              @"ip4": @"149.154.175.50",
                              @"ip6": @"2001:b28:f23d:f001::a",
                              @"port": @443
                              },
                      @2: @{
                              @"ip4": @"149.154.167.51",
                              @"ip6": @"2001:67c:4e8:f002::a",
                              @"port": @443
                              },
                      @3: @{
                              @"ip4": @"149.154.175.100",
                              @"ip6": @"2001:b28:f23d:f003::a",
                              @"port": @443
                              },
                      @4: @{
                              @"ip4": @"149.154.167.91",
                              @"ip6": @"2001:67c:4e8:f004::a",
                              @"port": @443
                              },
                      @5: @{
                              @"ip4": @"149.154.171.5",
                              @"ip6": @"2001:b28:f23f:f005::a",
                              @"port": @443
                              }
                      };
    }
    return addresses;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        MTApiEnvironment *apiEnvironment = [self makeApiEnvironment];
        [self setupContext:apiEnvironment keychain:self.keychain];
        
        [self moveToDatacenterId:2];
    }
    return self;
}

- (void)dealloc {
    if (self.requestService) {
        [self.mtProto removeMessageService:self.requestService];
    }
    [self.mtProto stop];
}

- (id<MTKeychain>)keychain {
    if (!_keychain) {
        NSString *keychainName = @"Telegram-temp";
        NSString *documentsPath = [RPC documentsPath];
        _keychain = [MTFileBasedKeychain keychainWithName:keychainName documentsPath:documentsPath];
    }
    
    return _keychain;
}

- (MTApiEnvironment *)makeApiEnvironment {
    MTApiEnvironment *apiEnvironment = [[MTApiEnvironment alloc] init];
    MTDatacenterAddress *address = [[MTDatacenterAddress alloc] initWithIp:@"149.154.167.40" port:443 preferForMedia:NO restrictToTcp:NO cdn:NO preferForProxy:NO];
    apiEnvironment.datacenterAddressOverrides = @{@1: address};
    apiEnvironment.apiId = apiId;
    apiEnvironment.layer = @([[[TGTLSerialization alloc] init] currentLayer]);
    
    return apiEnvironment;
}

- (void)setupContext:(MTApiEnvironment *)apiEnvironment keychain:(id<MTKeychain>)keychain {
    self.context = [[MTContext alloc] initWithSerialization:[[TGTLSerialization alloc] init] apiEnvironment:apiEnvironment];
    self.context.keychain = keychain;
    
//    [self.context addChangeListener:self];
    [self.context performBatchUpdates:^
     {
         for (NSNumber *key in [RPC seedAddressesForDatacenters]) {
             NSDictionary *info = [RPC seedAddressesForDatacenters][key];
             
             id ip4 = [[MTDatacenterAddress alloc] initWithIp:info[@"ip4"] port:[info[@"port"] integerValue] preferForMedia:NO restrictToTcp:NO cdn:NO preferForProxy:NO];
             id ip6 = [[MTDatacenterAddress alloc] initWithIp:info[@"ip6"] port:[info[@"port"] integerValue] preferForMedia:NO restrictToTcp:NO cdn:NO preferForProxy:NO];
             id list = @[ip4, ip6];
             id set = [[MTDatacenterAddressSet alloc] initWithAddressList:list];
             
             [self.context setSeedAddressSetForDatacenterWithId:key.integerValue seedAddressSet:set];
         }
     }];
}

- (NSObject *)performRpc:(TLMetaRpc *)rpc
         completionBlock:(void (^)(id<TLObject> response, int64_t responseTime, MTRpcError *error))completionBlock
           progressBlock:(void (^)(int length, float progress))__unused progressBlock
           quickAckBlock:(void (^)())quickAckBlock
      requiresCompletion:(bool)__unused requiresCompletion
            requestClass:(int)requestClass
            datacenterId:(int)datacenterId {
    
    MTRequest *request = [[MTRequest alloc] init];
    request.passthroughPasswordEntryError = requestClass & RPCRequestClassPassthroughPasswordNeeded;
    request.body = rpc;
    [request setCompleted:^(id result, NSTimeInterval timestamp, id error) {
        if (completionBlock != nil)
            completionBlock(result, (int64_t)(timestamp * 4294967296.0), error);
    }];
    
    if (quickAckBlock != nil) {
        [request setAcknowledgementReceived:quickAckBlock];
        [request setShouldDependOnRequest:^bool (MTRequest *anotherRequest) {
            return false;
        }];
    }

    bool continueOnFloodWait = true;
    if ([request.body isKindOfClass:[TLRPCauth_sendCode class]] || [request.body isKindOfClass:[TLRPCauth_resendCode$auth_resendCode class]]) {
        continueOnFloodWait = false;
    }
    
    [request setShouldContinueExecutionWithErrorContext:^bool(MTRequestErrorContext *errorContext)
     {
         if (!continueOnFloodWait && errorContext.floodWaitSeconds > 0) {
             return false;
         }
         if (requestClass & 256/*TGRequestClassFailOnServerErrors*/)
             return errorContext.internalServerErrorCount < 5;
         if (requestClass & 512)
             return false;
         return true;
     }];
    
    [self.requestService addRequest:request];
    
    return request.internalId;
}

- (void)moveToDatacenterId:(NSInteger)datacenterId
{
    if (_mtProto == nil || datacenterId != _mtProto.datacenterId)
    {
        [self resetMainMtProto:datacenterId];
    }
}

- (void)cancelRPC:(id)rpc {
    [_requestService removeRequestByInternalId:rpc];
}

- (void)resetMainMtProto:(NSInteger)datacenterId
{
    if (self.requestService != nil)
    {
        _requestService.delegate = nil;
        [_mtProto removeMessageService:_requestService];
        _requestService = nil;
    }

    if (_mtProto != nil)
    {
        _mtProto.delegate = nil;
        [_mtProto stop];
    }

    _mtProto = [[MTProto alloc] initWithContext:_context datacenterId:datacenterId usageCalculationInfo:nil];
//     _mtProto.delegate = self;

    _requestService = [[MTRequestMessageService alloc] initWithContext:_context];
//     _requestService.delegate = self;
    [_mtProto addMessageService:_requestService];

    [_context authInfoForDatacenterWithIdRequired:_mtProto.datacenterId isCdn:false];
}

@end
