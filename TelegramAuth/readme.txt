Установка

Добавить TelegramAuth проект в workspace
Добавить TelegramAuth.framework в Linked frameworks and libraries, а так же в Embed Frameworks в Build Phases
Добавить в bridging header

#import <TelegramAuth/TelegramAuth.h>

Использование

import TelegramAuth

Отправить сообщение на номер телефона

TelegramAuth.sharedInstance().sendCode(toPhone: "7(xxx) xxx-xx-xx", success: { (confirm) in
    localConfirm = confirm
    print("Success send code")
}) { (error) in
    print("Fail send code")
}

В success блок передается экземпляр TAConfirm
Когда прийдет код, в этот экземпляр (или его копию) в поле code нужно присвоить полученный код верификации. Далее эта структура будет использоваться при подтверждении.

В error.userInfo хранится 3 поля

kTelegramAuthErrorCode: TAErrorCode
kTelegramAuthErrorName: String - имя ошибки из документации телеграмма
kTelegramAuthErrorType: TAErrorType - типа метода (отправка/подтверждение)

Подтвердить код

localConfirm.code = receivedCode

TelegramAuth.sharedInstance().confirmCode(localConfirm, success: { (confirm) in
    //success
}) { (error) in
    //fail
}
