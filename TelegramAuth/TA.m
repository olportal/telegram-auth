//
//  TelegramAuth.m
//  telegram-temp
//
//  Created by Дмитрий Шелонин on 28/09/2017.
//  Copyright © 2017 Дмитрий Шелонин. All rights reserved.
//

#import "TA.h"
#import "Network.h"
#import "RPC.h"
#import "TLauth_SentCode$auth_sentCode.h"
#import "TLauth_SentCodeType.h"
#import "TLauth_Authorization$auth_authorization.h"
#import "TLUser$modernUser.h"

@implementation TAConfirm
@end

@implementation TelegramAuth

+ (instancetype)sharedInstance {
    static TelegramAuth *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [TelegramAuth new];
    });
    
    return sharedInstance;
}

- (TARequest *)sendCodeToPhone:(NSString *)phoneNumber success:(TASuccessBlock)success fail:(TAFailBlock)fail {
    TARequest *request = [TARequest new];
    [self _sendCodeToPhone:phoneNumber success:success fail:fail request:request];
    
    return request;
}

- (TARequest *)confirmCode:(TAConfirm *)confirmInfo success:(TASuccessBlock)success fail:(TAFailBlock)fail {
    TARequest *request = [TARequest new];
    [self _confirmCode:confirmInfo success:success fail:fail request:request];
    
    return request;
}

#pragma mark -

- (bool)isMigrateToDatacenterError:(NSString *)text datacenterId:(NSInteger *)datacenterId {
    NSArray *migratePrefixes = @[
                                 @"PHONE_MIGRATE_",
                                 @"NETWORK_MIGRATE_",
                                 @"USER_MIGRATE_"
                                 ];
    
    for (NSString *prefix in migratePrefixes)
    {
        NSRange range = [text rangeOfString:prefix];
        if (range.location != NSNotFound)
        {
            NSScanner *scanner = [[NSScanner alloc] initWithString:text];
            [scanner setScanLocation:range.location + range.length];
            
            NSInteger scannedDatacenterId = 0;
            if ([scanner scanInteger:&scannedDatacenterId] && scannedDatacenterId != 0)
            {
                if (datacenterId != NULL)
                {
                    *datacenterId = scannedDatacenterId;
                    
                    return true;
                }
            }
        }
    }
    
    return false;
}

- (void)_sendCodeToPhone:(NSString *)phoneNumber success:(TASuccessBlock)success fail:(TAFailBlock)fail request:(TARequest *)request {
    request.ident = [[Network shared] doSendConfirmationCode:phoneNumber success:^(TLauth_SentCode$auth_sentCode *object) {
        TAConfirm *confirm = [TAConfirm new];
        confirm.phone = phoneNumber;
        confirm.confirmedVia = TAConfirmViaOther;
        confirm.phoneHash = object.phone_code_hash;
        confirm.phoneRegistered = object.phone_registered;
        
        if ([object.type isKindOfClass:[TLauth_SentCodeType$auth_sentCodeTypeApp class]]) {
            confirm.confirmedVia = TAConfirmViaTelegram;
        } else if ([object.type isKindOfClass:[TLauth_SentCodeType$auth_sentCodeTypeCall class]]) {
            confirm.confirmedVia = TAConfirmViaPhone;
        }
        
        if (success) {
            dispatch_async(dispatch_get_main_queue(), ^{
                success(confirm);
            });
        }
    } fail:^(MTRpcError *error) {
        NSMutableDictionary *userInfo = [NSMutableDictionary new];
        
        NSString *errorType = error.errorDescription;
        userInfo[kTelegramAuthErrorType] = @(TAErrorTypeSendCode);
        userInfo[kTelegramAuthErrorName] = errorType;
        userInfo[kTelegramAuthErrorCode] = @(TAErrorCodeUnknown);
        
        if ([errorType isEqualToString:@"PHONE_NUMBER_INVALID"]) {
            userInfo[kTelegramAuthErrorCode] = @(TAErrorCodeInvalidPhoneNumber);
        } else if ([errorType hasPrefix:@"FLOOD_WAIT"]) {
            userInfo[kTelegramAuthErrorCode] = @(TAErrorCodeFloodWait);
        } else if ([errorType isEqualToString:@"PHONE_NUMBER_FLOOD"]) {
            userInfo[kTelegramAuthErrorCode] = @(TAErrorCodeFloodPhone);
        } else {
            NSInteger datacenterId = 0;
            if ([self isMigrateToDatacenterError:errorType datacenterId:&datacenterId] && datacenterId != 0 && request.resendCount == 0) {
                request.resendCount++;
                [[RPC shared] moveToDatacenterId:datacenterId];
                [self _sendCodeToPhone:phoneNumber success:success fail:fail request:request];
                return;
            }
        }
        
        if (fail) {
            dispatch_async(dispatch_get_main_queue(), ^{
                NSError *error = [NSError errorWithDomain:TAErrorDomain code:0 userInfo:userInfo];
                fail(error);
            });
        }
    }];
}

- (void)_confirmCode:(TAConfirm *)confirmInfo success:(TASuccessBlock)success fail:(TAFailBlock)fail request:(TARequest *)request {
    request.ident = [[Network shared] doSignIn:confirmInfo.phone phoneHash:confirmInfo.phoneHash phoneCode:confirmInfo.code success:^(id object) {
        if (success) {
            dispatch_async(dispatch_get_main_queue(), ^{
                TLUser$modernUser *user = ((TLauth_Authorization$auth_authorization *)object).user;
                TAConfirm *confirm = [TAConfirm new];
                confirm.userId = [NSString stringWithFormat:@"%d", user.n_id];
                confirm.userName = user.username;
                confirm.phone = confirmInfo.phone;
                success(confirm);
            });
        }
    } fail:^(MTRpcError *error) {
        NSMutableDictionary *userInfo = [NSMutableDictionary new];
        
        NSString *errorType = error.errorDescription;
        userInfo[kTelegramAuthErrorType] = @(TAErrorTypeConfirmCode);
        userInfo[kTelegramAuthErrorName] = errorType;
        userInfo[kTelegramAuthErrorCode] = @(TAErrorCodeUnknown);
        
        if ([errorType isEqualToString:@"PHONE_CODE_INVALID"]) {
            userInfo[kTelegramAuthErrorCode] = @(TAErrorCodeInvalidPhoneCode);
        } else if ([errorType hasPrefix:@"PHONE_CODE_EXPIRED"]) {
            userInfo[kTelegramAuthErrorCode] = @(TAErrorCodePhoneCodeExpired);
        } else if ([errorType isEqualToString:@"PHONE_NUMBER_UNOCCUPIED"]) {
            userInfo[kTelegramAuthErrorCode] = @(TAErrorCodePhoneNumberUnoccupied);
        } else if ([errorType isEqualToString:@"FLOOD_WAIT"]) {
            userInfo[kTelegramAuthErrorCode] = @(TAErrorCodeFloodWait);
        } else if ([errorType isEqualToString:@"SESSION_PASSWORD_NEEDED"]) {
            userInfo[kTelegramAuthErrorCode] = @(TAErrorCodeSessionPasswordNeeded);
        } else {
            NSInteger datacenterId = 0;
            if ([self isMigrateToDatacenterError:errorType datacenterId:&datacenterId] && datacenterId != 0 && request.resendCount == 0) {
                request.resendCount++;
                [[RPC shared] moveToDatacenterId:datacenterId];
                [self _confirmCode:confirmInfo success:success fail:fail request:request];
                
                return;
            }
        }
        
        if (fail) {
            dispatch_async(dispatch_get_main_queue(), ^{
                NSError *error = [NSError errorWithDomain:TAErrorDomain code:0 userInfo:userInfo];
                fail(error);
            });
        }
    }];
}

@end
