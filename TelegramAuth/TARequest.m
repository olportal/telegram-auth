//
//  TARequest.m
//  telegram-temp
//
//  Created by Дмитрий Шелонин on 28/09/2017.
//  Copyright © 2017 Дмитрий Шелонин. All rights reserved.
//

#import "TARequest.h"
#import "RPC.h"

@implementation TARequest

- (void)cancel {
    [[RPC shared] cancelRPC:self.ident];
}

@end
